﻿using System;
using SVNHooks.Core;

namespace SVNPostCommitHook
{
    class Program
    {

        static void Main(string[] args)
        {
            try
            {
                CommitSentinel sentinel = new CommitSentinel();

                sentinel.DocumentCommitOnJira(args[0], args[1], args[2]);
            }
            catch
            {
                Environment.Exit(1);
            }
        }
    }
}
