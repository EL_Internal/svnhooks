using SharpSvn;
using SVNHooks.Core.Exceptions;
using SVNHooks.Core.Interfaces;

namespace SVNHooks.Core
{
	/// <summary>
	/// Uses svn utilities to retrieve information from SVN Repository
	/// </summary>
	public class SvnRepository : ISvnRepository
	{
		#region Public Methods

		/// <summary>
		/// Gets the SVN revision info.
		/// </summary>
		/// <param name="transactionId">The transaction id.</param>
		/// <param name="repositoryPath">The repository path.</param>
		/// <returns></returns>
        public ISvnChangeInfoEventArgsWrapper GetSvnRevisionInfo(string transactionId, string repositoryPath, SvnHookType hookType)
		{
			using (SharpSvn.SvnLookClient svnLookClient = new SvnLookClient())
			{
				SvnChangeInfoEventArgs info;
                SvnHookArguments ha;
                SvnHookArguments.ParseHookArguments(new[]{repositoryPath,transactionId}, hookType, true, out ha);

                bool result = svnLookClient.GetChangeInfo(ha.LookOrigin, out info);

				if (result == false)
					throw new SvnLookupException(transactionId, repositoryPath);

				return new SvnChangeInfoEventArgsWrapper(info);
			}
		}

		#endregion
	}
}