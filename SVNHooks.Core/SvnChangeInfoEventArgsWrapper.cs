﻿using System.Collections.Generic;
using System.Linq;
using SharpSvn;
using SVNHooks.Core.Interfaces;

namespace SVNHooks.Core
{
	/// <summary>
	/// Wrapper around SharpSVN class required for mocking
	/// </summary>
	public class SvnChangeInfoEventArgsWrapper : ISvnChangeInfoEventArgsWrapper
	{
		#region Private Fields

		private readonly SvnChangeInfoEventArgs _revisionInfo;

		#endregion

		#region Constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="SvnChangeInfoEventArgsWrapper"/> class.
		/// </summary>
		/// <param name="revisionInfo">The <see cref="SharpSvn.SvnChangeInfoEventArgs"/> instance containing the event data.</param>
		public SvnChangeInfoEventArgsWrapper(SvnChangeInfoEventArgs revisionInfo)
		{
			this._revisionInfo = revisionInfo;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets the log message.
		/// </summary>
		/// <value>The log message.</value>
		public string LogMessage
		{
			get
			{
				return _revisionInfo.LogMessage;
			}
		}

		/// <summary>
		/// Gets the author.
		/// </summary>
		/// <value>The author.</value>
		public string Author
		{
			get
			{
				return _revisionInfo.Author;
			}
		}

		/// <summary>
		/// Gets the changed items.
		/// </summary>
		/// <value>The changed items.</value>
		public IList<SvnChangeItemWrapper> ChangedItems
		{
			get
			{
			    return this._revisionInfo.ChangedPaths.Select(changedPath => new SvnChangeItemWrapper(changedPath.Path, changedPath.Action)).ToList();
			}
		}

		#endregion
	}
}