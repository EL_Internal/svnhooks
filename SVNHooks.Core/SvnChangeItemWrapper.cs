using SharpSvn;

namespace SVNHooks.Core
{
	/// <summary>
	/// Wrapper around SharpSVN class required for mocking
	/// </summary>
	public class SvnChangeItemWrapper
	{
		#region Properties

		/// <summary>
		/// Gets or sets the path.
		/// </summary>
		/// <value>The path.</value>
		public string Path { get; set; }

		/// <summary>
		/// Gets or sets the action.
		/// </summary>
		/// <value>The action.</value>
		public SvnChangeAction Action { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SvnChangeItemWrapper"/> class.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <param name="action">The action.</param>
		public SvnChangeItemWrapper(string path, SvnChangeAction action)
		{
			this.Path = path;
			this.Action = action;
		}

		#endregion
	}
}