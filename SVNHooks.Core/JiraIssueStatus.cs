namespace SVNHooks.Core
{
	public enum JiraIssueStatus
	{
		Unknown = 0,
		Open = 1,
		InProgress = 3,
		ReOpened = 4,
		Resolved = 5,
		Closed = 6,
		Reviewed = 10000        
	}
}