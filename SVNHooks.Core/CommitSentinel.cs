using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text.RegularExpressions;
using log4net;
using log4net.Config;
using SharpSvn;
using SVNHooks.Core.Configuration;
using SVNHooks.Core.Interfaces;
using SVNHooks.Core.JIRA;

namespace SVNHooks.Core
{
	/// <summary>
	/// Determines whether a commit should be allowed or prevented based on the comment.
	/// </summary>
	public class CommitSentinel
	{
		#region Private Members

		private readonly ISvnRepository _repository = new SvnRepository();
		private readonly IJiraGateway _gateway = new JiraGateway();
		private readonly ILog _log = LogManager.GetLogger("CommitSentinel");

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="CommitSentinel"/> class.
		/// </summary>
		public CommitSentinel()
		{
			this.ExemptionSettings = CommitPolicyConfigurationSectionHandler.CurrentSettings.CommitExemptionSettings;
            XmlConfigurator.Configure(new FileInfo("SVNPreCommitHook.Console.exe.config"));
		}

		/// <summary>
		/// Overload used for testing.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="gateway">The gateway.</param>
		/// <param name="log">The log.</param>
		public CommitSentinel(ISvnRepository repository, IJiraGateway gateway, ILog log)
			: this(repository, gateway, log, CommitPolicyConfigurationSectionHandler.CurrentSettings.CommitExemptionSettings)
		{
		}

		/// <summary>
		/// Overload used for testing.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="gateway">The gateway.</param>
		/// <param name="log">The log.</param>
		/// <param name="exemptionSettings">The exemption settings.</param>
		public CommitSentinel(ISvnRepository repository,
		                      IJiraGateway gateway,
		                      ILog log,
		                      CommitExemptionSettings exemptionSettings
			)
		{
			this.ExemptionSettings = exemptionSettings;
			this._repository = repository;
			this._gateway = gateway;
			this._log = log;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the exemption settings.
		/// </summary>
		/// <value>The exemption settings.</value>
		public CommitExemptionSettings ExemptionSettings { get; private set; }

		#endregion

		#region Public Methods

	    /// <summary>
	    /// Called by console program to determine whether a commit should proceeed.
	    /// </summary>
	    /// <param name="svnRepositoryPath">The SVN path.</param>
	    /// <param name="svnTransaction">The SVN transaction.</param>
        /// <param name="jiraKey">Key of JIRA project being monitored.</param>
	    /// <returns></returns>
	    public CommitResult DetermineCommitStatus(string svnRepositoryPath, string svnTransaction, string jiraKey="")
		{
			try
			{
				ISvnChangeInfoEventArgsWrapper revisionInfo = this._repository.GetSvnRevisionInfo(svnTransaction, svnRepositoryPath,SvnHookType.PreCommit);
				SvnLogMessage logMessage = new SvnLogMessage(revisionInfo.LogMessage);

				if (this.ContainsOnlyExemptChanges(revisionInfo.ChangedItems, revisionInfo.Author))
					return new CommitResult(true);

				return this.ContainsValidJiraIssueInLogMessage(logMessage, jiraKey);
			}
			catch (Exception e)
			{
				_log.Debug(string.Format("Error in determining commit status for path: {0}, transactionId: {1}. Error Message: {2}; Stack Trace: {3}", svnTransaction, svnRepositoryPath, e.Message, e.StackTrace));

				return new CommitResult(false, e.Message);
			}
		}

        public bool DocumentCommitOnJira(string svnRepositoryPath, string svnRevision, string jiraKey)
        {
            try
            {
                _log.Debug("Path: "+svnRepositoryPath+"\nRevision: "+svnRevision);
                ISvnChangeInfoEventArgsWrapper revisionInfo = this._repository.GetSvnRevisionInfo(svnRevision, svnRepositoryPath, SvnHookType.PostCommit);

                string changedFiles = "";

                if (revisionInfo.ChangedItems.Count > 0)
                { 
                    changedFiles = "\n\nChange Log:";

                    foreach (SvnChangeItemWrapper change in revisionInfo.ChangedItems)
                    {
                        changedFiles += "\n" + change.Action.ToString() + ": " + change.Path; 
                    }
                }

                SvnLogMessage logMessage = new SvnLogMessage(revisionInfo.LogMessage);

                _gateway.AddComment(logMessage.GetCandidateJiraKey(jiraKey), new RemoteComment() { body = (logMessage.LogMessage+"\n\nRevision "+svnRevision+" by "+revisionInfo.Author + changedFiles) });

                return true;
            }
            catch (Exception e)
            {
                _log.Info(string.Format("Error in documenting commit to Jira for path: {0}, transactionId: {1}. Error Message: {2}; Stack Trace: {3}", svnRevision, svnRepositoryPath, e.Message, e.StackTrace));

                return false;
            }
        }

		#endregion

		#region Private Methods

	    /// <summary>
	    /// Determines whether [contains valid jira issue] [the specified log message].
	    /// </summary>
	    /// <param name="logMessage">The log message.</param>
	    /// <param name="jiraKey">Key of JIRA project being monitored.</param>
	    /// <returns></returns>
	    private CommitResult ContainsValidJiraIssueInLogMessage(SvnLogMessage logMessage, string jiraKey)
		{
			string candidateKey = logMessage.GetCandidateJiraKey(jiraKey);

			if (String.IsNullOrEmpty(candidateKey))
				return new CommitResult(false, string.Format("The commit log message did not contain a candidate JIRA key from one of the following projects: {0}", jiraKey.Replace('|', ',')));

			RemoteIssue issue = null;
			try
			{
				issue = this._gateway.GetJIRAIssue(candidateKey);
			}
			catch (FaultException)
			{
				return new CommitResult(false, string.Format("The key {0} was not found in JIRA.", candidateKey));
			}
			catch (Exception e)
			{
				_log.Info(string.Format("Unexpected error occurred when looking up Jira Issue: {0}; {1}", e.Message, e.StackTrace));
			}

			if (issue == null)
				return new CommitResult(false, "A null object was returned from JIRA. Escalate to SVN Admin.");

			if (JiraGateway.GetIssueStatus(issue) == JiraIssueStatus.Closed ||
			    (JiraGateway.GetIssueStatus(issue) == JiraIssueStatus.Reviewed))
				return new CommitResult(false, string.Format("The Jira issue {0} was in a {1} state. You must enter an issue that has either an opened, in-progress, resolved, or reopened status.", candidateKey, JiraGateway.GetIssueStatus(issue)));

			if (CommitSentinel.ContainsAReleasedFixVersion(issue.fixVersions) == true)
				return new CommitResult(false, string.Format("The Jira issue {0} has a {1} fix version, which was already released. You must enter an issue with a fix version that is not in a released state.", candidateKey, issue.fixVersions[0].name));

			return new CommitResult(true);
		}

		/// <summary>
		/// Determines whether [has at least one released fix version] [the specified versions].
		/// </summary>
		/// <param name="versions">The versions.</param>
		/// <returns>
		/// 	<c>true</c> if [has at least one released fix version] [the specified versions]; otherwise, <c>false</c>.
		/// </returns>
		private static bool ContainsAReleasedFixVersion(IEnumerable<RemoteVersion> versions)
		{
		    return versions.Any(version => version.released);
		}


	    /// <summary>
		/// Determines whether [contains only exempt target files] [the specified changed paths].
		/// </summary>
		/// <param name="changedPaths">The changed paths.</param>
		/// <param name="author">The author.</param>
		/// <returns>
		/// 	<c>true</c> if [contains only exempt target files] [the specified changed paths]; otherwise, <c>false</c>.
		/// </returns>
		private bool ContainsOnlyExemptChanges(IEnumerable<SvnChangeItemWrapper> changedPaths, string author)
		{
			foreach (SvnChangeItemWrapper changeItem in changedPaths)
			{
				if (this.MatchesExemptionRule(changeItem.Path, author))
					continue;

				_log.Debug(string.Format("The following author and path combination doesn't contain a RegEx exemption match: {0} - {1}", author, changeItem.Path));
				return false;
			}

			return true;
		}

		/// <summary>
		/// Matcheses the exemption rule.
		/// </summary>
		/// <param name="targetPath">The target path.</param>
		/// <param name="author">The author.</param>
		/// <returns></returns>
		public bool MatchesExemptionRule(string targetPath, string author)
		{
		    return
		        CommitPolicyConfigurationSectionHandler.CurrentSettings.CommitExemptionSettings.ExemptItems.Any(
		            exemptItem =>
		            MatchesPathExemption(targetPath, exemptItem.RegularExpressionForPath) &&
		            MatchesUserExemption(author, exemptItem.Users));
		}

	    /// <summary>
		/// Pathes the matches exemption.
		/// </summary>
		/// <param name="targetPath">The target path.</param>
		/// <param name="regularExpression">The regular expression.</param>
		/// <returns></returns>
		private static bool MatchesPathExemption(string targetPath, string regularExpression)
		{
			return Regex.Match(targetPath, regularExpression).Success;
		}

		/// <summary>
		/// Matcheses the user exemption.
		/// </summary>
		/// <param name="author">The author.</param>
		/// <param name="exemptUsers">The exempt users.</param>
		/// <returns></returns>
		private static bool MatchesUserExemption(string author, IEnumerable<User> exemptUsers)
		{
		    return exemptUsers.Any(user => user.Name == author || user.Name == "*");
		}

		#endregion
	}
}