using System.Collections.Generic;

namespace SVNHooks.Core.Interfaces
{
	/// <summary>
	/// Uses svn utilities to retrieve information from SVN Repository
	/// </summary>
	public interface ISvnChangeInfoEventArgsWrapper
	{
		#region Properties

		/// <summary>
		/// Gets the log message.
		/// </summary>
		/// <value>The log message.</value>
		string LogMessage { get; }

		/// <summary>
		/// Gets the author.
		/// </summary>
		/// <value>The author.</value>
		string Author { get; }

		/// <summary>
		/// Gets the changed items.
		/// </summary>
		/// <value>The changed items.</value>
		IList<SvnChangeItemWrapper> ChangedItems { get; }

		#endregion
	}
}