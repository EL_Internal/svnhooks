using SharpSvn;

namespace SVNHooks.Core.Interfaces
{
	/// <summary>
	/// Uses svn utilities to retrieve information from SVN Repository
	/// </summary>
	public interface ISvnRepository
	{
		#region Public Methods

		/// <summary>
		/// Gets the SVN revision info.
		/// </summary>
		/// <param name="transactionId">The transaction id.</param>
		/// <param name="repositoryPath">The repository path.</param>
		/// <returns></returns>
		ISvnChangeInfoEventArgsWrapper GetSvnRevisionInfo(string transactionId, string repositoryPath, SvnHookType hookType);

		#endregion
	}
}