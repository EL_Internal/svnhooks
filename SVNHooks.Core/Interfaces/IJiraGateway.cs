using SVNHooks.Core.JIRA;

namespace SVNHooks.Core.Interfaces
{
	/// <summary>
	/// Communicates with Jira web service
	/// </summary>
	public interface IJiraGateway
	{
		#region Public Methods

		/// <summary>
		/// Gets the JIRA issue.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		RemoteIssue GetJIRAIssue(string key);

	    void AddComment(string key, RemoteComment remoteComment);

	    #endregion
	}
}