using System;
using System.Configuration;
using System.Text.RegularExpressions;

namespace SVNHooks.Core
{
	/// <summary>
	/// The comment entered by developer when doing a commit
	/// </summary>
	public class SvnLogMessage
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SvnLogMessage"/> class.
		/// </summary>
		/// <param name="logMessage">The log message.</param>
		public SvnLogMessage(string logMessage)
		{
			this.LogMessage = logMessage;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets the log message from the attempted SVN Commit.
		/// </summary>
		/// <value>The log message.</value>
		public string LogMessage { get; private set; }

		#endregion

		#region Public Methods

		/// <summary>
		/// Parses log message and returns the first match that contain the monitored project key strings
		/// listed in the app.config file
		/// </summary>
		/// <returns></returns>
		public string GetCandidateJiraKey(string jiraKey)
		{
			string RegExExpression = string.Format(@"(({0})-\w*)",jiraKey);
			Regex exp = new Regex(RegExExpression, RegexOptions.IgnoreCase);

			MatchCollection MatchList = exp.Matches(this.LogMessage);
            
			if (MatchList.Count > 0)
				return MatchList[0].Value.ToUpper().Trim();

			return "";
		}

        /// <summary>
        ///         
        /// </summary>
        /// <returns></returns>
        public string GetWorkLogged(string jiraKey)
        {
            throw new NotImplementedException("Regex needs to be written to parse time spent.");
            
            //string RegExExpression = "";
            //Regex exp = new Regex(RegExExpression, RegexOptions.IgnoreCase);

            //MatchCollection MatchList = exp.Matches(this.LogMessage);

            //if (MatchList.Count > 0)
            //    return MatchList[0].Value.ToUpper().Trim();

            //return "";
        }

  

		#endregion
	}
}