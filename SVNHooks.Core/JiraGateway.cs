﻿using System;
using System.Configuration;
using SVNHooks.Core.Interfaces;
using SVNHooks.Core.JIRA;

namespace SVNHooks.Core
{
	/// <summary>
	/// Communicates with Jira web service
	/// </summary>
	public class JiraGateway : IJiraGateway
	{
		#region Constants
		//public const string MONITORED_JIRA_KEYS_CONFIG_KEY = "MonitoredJiraKeys";
		//public static readonly string MONITORED_PROJECTS = ConfigurationManager.AppSettings[MONITORED_JIRA_KEYS_CONFIG_KEY];

		#endregion

		#region Private Members

		private readonly JiraSoapServiceClient _service = new JiraSoapServiceClient();
		private static readonly string JiraUserName = ConfigurationManager.AppSettings["JiraUserName"];
		private static readonly string JiraPassword = ConfigurationManager.AppSettings["JiraPassword"];

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="JiraGateway"/> class.
		/// </summary>
		public JiraGateway()
		{
		}

		/// <summary>
		/// Overload used for mocking in unit tests
		/// </summary>
		/// <param name="service">The service.</param>
		public JiraGateway(JiraSoapServiceClient service)
		{
			this._service = service;
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Gets the JIRA issue from key (i.e. TANT-1212).
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public RemoteIssue GetJIRAIssue(string key)
		{
			return _service.getIssue(this.Login(), key);
		}

		/// <summary>
		/// Gets the issue status. Returns unknown if can't be parsed.
		/// </summary>
		/// <param name="issue">The issue.</param>
		/// <returns></returns>
		public static JiraIssueStatus GetIssueStatus(RemoteIssue issue)
		{
			JiraIssueStatus status;
			try
			{
				status = (JiraIssueStatus)Int32.Parse(issue.status);
			}
			catch (Exception)
			{
				status = JiraIssueStatus.Unknown;
			}
			return status;
		}

        public void AddComment(string jiraKey, RemoteComment comment)
        {
            this._service.addComment(Login(), jiraKey, comment);
        }

		#endregion

		#region Private Methods

		/// <summary>
		/// Logins this instance.
		/// </summary>
		/// <returns>token that used in first parameter of all web requests</returns>
		private string Login()
		{
			return this._service.login(JiraUserName, JiraPassword);
		}

		#endregion
	}
}