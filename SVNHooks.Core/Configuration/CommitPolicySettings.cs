using System;
using System.Xml.Serialization;

namespace SVNHooks.Core.Configuration
{
	/// <summary>
	/// The rool level configuration class that contains all the settings
	/// that determine whether or not a revision can be checked into repository
	/// </summary>
	[Serializable]
	[XmlRoot("CommitPolicySettings")]
	public class CommitPolicySettings
	{
		#region Contructors

		/// <summary>
		/// Initializes a new instance of the <see cref="CommitPolicySettings"/> class.
		/// </summary>
		public CommitPolicySettings()
		{
			this.CommitExemptionSettings = new CommitExemptionSettings();
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the commit exemption settings.
		/// </summary>
		/// <value>The commit exemption settings.</value>
		public CommitExemptionSettings CommitExemptionSettings { get; set; }

		#endregion
	}
}