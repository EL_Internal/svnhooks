using System;

namespace SVNHooks.Core.Configuration
{
	/// <summary>    
	/// The class that contains all the settings from the CommitExemptionSettings node
	/// of the app.config file which is used to determine whether or not a revision can be checked into repository  
	/// </summary>
	[Serializable]
	public class CommitExemptionSettings
	{
		#region Properties

		/// <summary>
		/// Gets or sets the combinations of paths and users that can be committed to 
		/// repository without a comment (usually through build process).
		/// </summary>
		/// <value>The exempt items.</value>
		public ExemptItem[] ExemptItems { get; set; }

		#endregion
	}
}