using System;
using System.Xml.Serialization;

namespace SVNHooks.Core.Configuration
{
    /// <summary>
    /// Represents an author that was specified in the app.config
    /// file that when combined with an allowable path is allowed to commit 
    /// to the repository without a comment (usually through build process)
    /// </summary>
    [Serializable]
    public class User
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        public User()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public User(string name)
        {
            this.Name = name;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name of the person attempting to
        /// commit a revision to the repository.
        /// </summary>
        /// <value>The name.</value>
        [XmlAttribute("name")]
        public string Name { get; set; }

        #endregion
    }
}