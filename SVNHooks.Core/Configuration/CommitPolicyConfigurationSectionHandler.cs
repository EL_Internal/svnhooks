﻿using System;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;

namespace SVNHooks.Core.Configuration
{
	/// <summary>
	/// The configuration file handler that loads all the settings from the CommitPolicy node in the app.config
	/// which are used to determine whether or not a revision can be checked into repository
	/// </summary>
	public class CommitPolicyConfigurationSectionHandler: IConfigurationSectionHandler
	{
		#region Constants

		private const string COMMIT_POLICY_SETTINGS_KEY = "CommitPolicySettings";

		#endregion

		#region Properties

		/// <summary>
		/// Gets the current settings.
		/// </summary>
		/// <value>The current settings.</value>
		public static CommitPolicySettings CurrentSettings
		{
			get
			{
				CommitPolicySettings settings = ConfigurationManager.GetSection(CommitPolicyConfigurationSectionHandler.COMMIT_POLICY_SETTINGS_KEY) as CommitPolicySettings;
				return settings ?? new CommitPolicySettings();
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Creates a configuration section handler.
		/// </summary>
		/// <param name="parent">Parent object.</param>
		/// <param name="configContext">Configuration context object.</param>
		/// <param name="section">Section XML node.</param>
		/// <returns>The created section handler object.</returns>
		public object Create(object parent, object configContext, XmlNode section)
		{
			if (section == null)
				return null;

			try
			{
				XmlSerializer serializer = new XmlSerializer(typeof(CommitPolicySettings));
				XmlReader reader = new XmlNodeReader(section);
				CommitPolicySettings settings = (CommitPolicySettings)serializer.Deserialize(reader);

				return settings;
			}
			catch (Exception ex)
			{
				throw new ApplicationException("Could not deserialize the configuration values for the commit policy settings.", ex);
			}
		}

		#endregion
	}
}