using System;
using System.Xml.Serialization;
using SVNHooks.Core.Configuration;

namespace SVNHooks.Core.Configuration
{
	/// <summary>
	/// Represents a target path\author combination setting from the app.config
	/// file that is allowed to commit to the repository without a comment 
	/// (usually through build process)
	/// </summary>
	[Serializable]
	public class ExemptItem
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ExemptItem"/> class.
		/// </summary>
		public ExemptItem()
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ExemptItem"/> class.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="regularExpressionForPath">The regular expression for path.</param>
		/// <param name="users">The users.</param>
		public ExemptItem(string name, string regularExpressionForPath, User[] users)
		{
			this.Name = name;
			this.RegularExpressionForPath = regularExpressionForPath;
			this.Users = users;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>The name.</value>
		[XmlAttribute("name")]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the regular expression for path.
		/// </summary>
		/// <value>The regular expression for path.</value>
		public string RegularExpressionForPath { get; set; }

		/// <summary>
		/// Gets or sets the users.
		/// </summary>
		/// <value>The users.</value>
		public User[] Users { get; set; }

		#endregion
	}
}