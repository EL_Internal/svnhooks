namespace SVNHooks.Core
{
	/// <summary>
	/// Result returned from CommitSentinel
	/// </summary>
	public class CommitResult
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="CommitResult"/> class.
		/// </summary>
		/// <param name="isValid">if set to <c>true</c> [is valid].</param>
		public CommitResult(bool isValid)
			: this(isValid, "")
		{}

		/// <summary>
		/// Initializes a new instance of the <see cref="CommitResult"/> class.
		/// </summary>
		/// <param name="isValid">if set to <c>true</c> [is valid].</param>
		/// <param name="errorMessage">The error message.</param>
		public CommitResult(bool isValid, string errorMessage)
		{
			this.IsValid = isValid;
			this.ErrorMessage = errorMessage;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets a value indicating whether or not commit can take place.
		/// </summary>
		/// <value><c>true</c> if this instance is valid commit takes place; otherwise commit not allowed
		public bool IsValid { get; private set; }

		/// <summary>
		/// Gets the error message.
		/// </summary>
		/// <value>The error message.</value>
		public string ErrorMessage { get; private set; }

		#endregion
	}
}