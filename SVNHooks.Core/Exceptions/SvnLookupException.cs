using System;

namespace SVNHooks.Core.Exceptions
{
	/// <summary>
	/// Exception thrown when unable to retrieve revision information from SVNLookup
	/// </summary>
	public class SvnLookupException : ApplicationException
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SvnLookupException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		public SvnLookupException(string message): base(message)
		{}

		/// <summary>
		/// Initializes a new instance of the <see cref="SvnLookupException"/> class.
		/// </summary>
		/// <param name="transactionId">The transaction id.</param>
		/// <param name="repositoryPath">The repository path.</param>
		public SvnLookupException(string transactionId, string repositoryPath):
			base(string.Format("Unable to retrieve log information for revision path: {0} and {1}.", transactionId, repositoryPath))
		{}
	}
}