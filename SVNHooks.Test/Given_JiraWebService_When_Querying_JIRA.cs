﻿using System.ServiceModel;
using MbUnit.Framework;
using SVNHooks.Core;
using SVNHooks.Core.JIRA;

namespace TestSVNHooks
{
    /// <summary>
    /// Tests against live JIRA web service
    /// </summary>
    [TestFixture]
    [FixtureCategory("TestJiraServer")]
    [Author("Russell Ball")]
    public class Given_JiraWebService_When_Querying_JIRA
    {
        #region Tests

        /// <summary>
        /// Should retrieve issue.
        /// </summary>
        [Test, Ignore("Integration test")]
        public void Should_Retrieve_Closed_Issue()
        {
            const string testKey = "LIB-1587";
            JiraGateway gateway = new JiraGateway();
            RemoteIssue issue = gateway.GetJIRAIssue(testKey);
            Assert.AreEqual(testKey, issue.key);
            Assert.AreEqual(JiraIssueStatus.Closed, JiraGateway.GetIssueStatus(issue));
        }

        /// <summary>
        /// Should throw exception when bogus issue.
        /// </summary>
        [Test, ExpectedException(typeof(FaultException))]
        public void Should_ThrowException_When_Bogus_Issue()
        {
            const string testKey = "TA";
            JiraGateway gateway = new JiraGateway();
            gateway.GetJIRAIssue(testKey);
        }

        #endregion
    }
}
