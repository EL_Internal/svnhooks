﻿using System.Configuration;
using MbUnit.Framework;
using SharpSvn;

namespace TestSVNHooks
{
    [TestFixture]
    [FixtureCategory("TestSvnRepository")]
    [Author("Russell Ball")]
    class Given_TestSvnRepository_When_Querying_SVN
    {
        private readonly string SVN_REPOSITORY_PATH = ConfigurationManager.AppSettings["SvnRepositoryPath"];

        /// <summary>
        /// Temp_sharpsvns this instance.
        /// </summary>
		[Test, Ignore("Integration test")]
        public void test_svn_lookup_manually()
        {
            using (SharpSvn.SvnLookClient svnLookClient = new SvnLookClient())
            {
                SvnChangeInfoEventArgs info;            	;
            	bool result = svnLookClient.GetChangeInfo(new SvnLookOrigin(SVN_REPOSITORY_PATH), new SvnChangeInfoArgs(), out info);
                Assert.IsTrue(result);
                Assert.AreEqual(info.Revision, 99206);
                Assert.AreEqual(info.Author, "rball");
                Assert.AreEqual(info.LogMessage, "tant-8870");
                Assert.AreEqual(info.ChangedPaths.Count, 1);
                Assert.AreEqual(info.ChangedPaths[0].Action, SvnChangeAction.Modify);
                Assert.AreEqual(info.ChangedPaths[0].Path, "/trunk/some/hack.txt");
            }
        }
    }
}
