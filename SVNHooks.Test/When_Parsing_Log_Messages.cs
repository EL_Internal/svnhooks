using System.Collections;
using MbUnit.Framework;
using SVNHooks.Core;

namespace TestSVNHooks
{
    /// <summary>
    /// Tests logic to parse jira issue keys from log messages
    /// </summary>
    [TestFixture]
    [FixtureCategory("CI")]
    [Author("Russell Ball")]
    public class When_Parsing_Log_Messages
    {
        #region Tests

        /// <summary>
        /// Verifies that keys are correctly extracted from message strings.
        /// </summary>
        /// <param name="logMessage">The log message.</param>
        /// <param name="candidate">The candidate.</param>
        [RowTest]
        [Row("blah blah TANT-323 blah", "TANT-323")]
        [Row("TANT-323 blah", "TANT-323")]
        [Row("TANT-323", "TANT-323")]
        [Row("tant-323", "TANT-323")]
        [Row("PROJ-323", "PROJ-323")]
        [Row("blah PROJ-323 blah", "PROJ-323")]
        public void Should_Parse_Candidate(string logMessage, string candidate)
        {
            SvnLogMessage message = new SvnLogMessage(logMessage);
            Assert.AreEqual(candidate, message.GetCandidateJiraKey("TANT|PROJ"));
        }     

        #endregion
    }
}
