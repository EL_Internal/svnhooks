using System;
using System.Collections.Generic;
using System.ServiceModel;
using log4net;
using MbUnit.Framework;
using Rhino.Mocks;
using SharpSvn;
using SVNHooks.Core;
using SVNHooks.Core.Configuration;
using SVNHooks.Core.Interfaces;
using SVNHooks.Core.JIRA;

namespace TestSVNHooks
{
    /// <summary>
    /// Tests logic that determines whether a revision should be committed
    /// </summary>
    [TestFixture]
    [FixtureCategory("CI")]
    [Author("Russell Ball")]
    public class When_Determining_Commit_Status
    {
        #region Constants

        private const string TEST_REPOSITORY = @"C:\myRepos\mycode.cs";
        private const string TEST_TRANSACTION_ID = "22-22";

        #endregion

        #region Tests

        /// <summary>
        /// issue exists so allows
        /// </summary>
        [Test]
        public void Should_Return_Valid_Status_When_Issue_Exists_And_Active()
        {
			//arrange
            ISvnRepository repository = this.CreateRepositoryStub("blah PROJ-111 blah");
            IJiraGateway gateway = this.CreateJiraGatewayStub();
            CommitSentinel sentinel = this.CreateCommitSentinel(repository, gateway);

			//act
            CommitResult result = sentinel.DetermineCommitStatus(TEST_TRANSACTION_ID, TEST_REPOSITORY);

			//assert
            Assert.IsTrue(result.IsValid);
        }

        /// <summary>
        /// Issue doesn't exist, so prevents commit
        /// </summary>
        [Test]
        public void Should_Return_Invalid_Status_When_Issue_Does_NOT_Exist()
        {
            //arrange
            ISvnRepository repository = this.CreateRepositoryStub("blah PROJ-111 blah");
            IJiraGateway gateway = MockRepository.GenerateStub<IJiraGateway>();
            gateway.Stub(x => x.GetJIRAIssue("PROJ-111")).Throw(new FaultException());
			CommitSentinel sentinel = this.CreateCommitSentinel(repository, gateway);

            //act            
            CommitResult result = sentinel.DetermineCommitStatus(TEST_TRANSACTION_ID, TEST_REPOSITORY,"PROJ");

            //assert
            Assert.IsFalse(result.IsValid);
        }

        /// <summary>
        /// no candidate keys in log message, so prevents commit
        /// </summary>
        [Test]
        public void Should_Return_Invalid_Status_When_No_Parsable_Issue()
        {
            //arrange
            ISvnRepository repository = this.CreateRepositoryStub("blah blah");
            IJiraGateway gateway = this.CreateJiraGatewayStub();
			CommitSentinel sentinel = this.CreateCommitSentinel(repository, gateway);

            //act            
            CommitResult result = sentinel.DetermineCommitStatus(TEST_TRANSACTION_ID, TEST_REPOSITORY,"");

            //assert
            Assert.IsFalse(result.IsValid);
        }

        /// <summary>
        /// issue exists, but closed so prevents commit
        /// </summary>
        [RowTest]
        [Row(JiraIssueStatus.Closed)]
        [Row(JiraIssueStatus.Reviewed)]
        public void Should_Return_Invalid_Status_When_Issue_Exists_And_InvalidStatus(JiraIssueStatus invalidStatus)
        {
            //arrange
            ISvnRepository repository = this.CreateRepositoryStub("blah PROJ-111 blah");
            IJiraGateway gateway = this.CreateJiraGatewayStub(invalidStatus);
			CommitSentinel sentinel = this.CreateCommitSentinel(repository, gateway);

            //act           
            CommitResult result = sentinel.DetermineCommitStatus(TEST_TRANSACTION_ID, TEST_REPOSITORY);

            //assert
            Assert.IsFalse(result.IsValid);
        }

        /// <summary>
        /// no log message, but commits because contains only exempt files
        /// </summary>
        [Test]
        public void Should_Return_Valid_Status_When_Contains_Exempt_File_And_Exempt_User()
        {
            //arrange
            ISvnRepository repository = this.CreateRepositoryStub("", "ExemptUser", new List<SvnChangeItemWrapper> { new SvnChangeItemWrapper(@"trunk/Src/Path/To/Some/Auto/Updated/File.cs", SvnChangeAction.Modify) });
            IJiraGateway gateway = this.CreateJiraGatewayStub();
			CommitSentinel sentinel = this.CreateCommitSentinel(repository, gateway);

            //act
            CommitResult result = sentinel.DetermineCommitStatus(TEST_TRANSACTION_ID, TEST_REPOSITORY,"");

            //assert
            Assert.IsTrue(result.IsValid);
        }

        /// <summary>
        /// exempt file, but commits fails because of non-exempt user
        /// </summary>
        [Test]
        public void Should_Return_Invalid_Status_When_Contains_Exempt_File_And_NonExempt_User()
        {
            //arrange
            ISvnRepository repository = this.CreateRepositoryStub("", "rball", new List<SvnChangeItemWrapper> { new SvnChangeItemWrapper(@"trunk/Src/Path/To/Some/Auto/Updated/File.cs", SvnChangeAction.Modify) });
            IJiraGateway gateway = this.CreateJiraGatewayStub();
			CommitSentinel sentinel = this.CreateCommitSentinel(repository, gateway);

            //act            
            CommitResult result = sentinel.DetermineCommitStatus(TEST_TRANSACTION_ID, TEST_REPOSITORY);

            //assert
            Assert.IsFalse(result.IsValid);
        }

        /// <summary>
        /// no log message and contains one exempt file, but also contains non-exempt file so prevents commit because no jira issue present
        /// </summary>
        [Test]
        public void Should_Return_Invalid_Status_When_Contains_Both_Exempt_And_NonExempt_Files()
        {
            //arrange
            IList<SvnChangeItemWrapper> changedPaths = new List<SvnChangeItemWrapper>
                                                           {
                                                               new SvnChangeItemWrapper(@"trunk/Src/Path/To/Some/Auto/Updated/File.cs", SvnChangeAction.Modify),
                                                               new SvnChangeItemWrapper(@"trunk/path/somefile.cs", SvnChangeAction.Modify),
                                                           };
            ISvnRepository repository = this.CreateRepositoryStub("", "ExemptUser", changedPaths);
            IJiraGateway gateway = this.CreateJiraGatewayStub();
			CommitSentinel sentinel = this.CreateCommitSentinel(repository, gateway);

            //act            
            CommitResult result = sentinel.DetermineCommitStatus(TEST_TRANSACTION_ID, TEST_REPOSITORY);

            //assert
            Assert.IsFalse(result.IsValid);
        }

        [Test]
        public void Should_Return_Valid_Status_When_Contains_Exempt_Directory_And_Exempt_User()
        {
            //arrange
            ISvnRepository repository = this.CreateRepositoryStub("", "ExemptUser", new List<SvnChangeItemWrapper> { new SvnChangeItemWrapper(@"tags/some/file.cs", SvnChangeAction.Modify) });
            IJiraGateway gateway = this.CreateJiraGatewayStub();
			CommitSentinel sentinel = this.CreateCommitSentinel(repository, gateway);

            //act
            CommitResult result = sentinel.DetermineCommitStatus(TEST_TRANSACTION_ID, TEST_REPOSITORY);

            //assert
            Assert.IsTrue(result.IsValid);
        }

        /// <summary>
        /// exempt directory, but commits fails because of non-exempt user
        /// </summary>
        [Test]
        public void Should_Return_Invalid_Status_When_Contains_Exempt_Directory_And_NonExempt_User()
        {
            //arrange
            ISvnRepository repository = this.CreateRepositoryStub("", "rball", new List<SvnChangeItemWrapper> { new SvnChangeItemWrapper(@"tags/2009.1.0.1/", SvnChangeAction.Modify) });
            IJiraGateway gateway = this.CreateJiraGatewayStub();
			CommitSentinel sentinel = this.CreateCommitSentinel(repository, gateway);

            //act
            CommitResult result = sentinel.DetermineCommitStatus(TEST_TRANSACTION_ID, TEST_REPOSITORY);

            //assert
            Assert.IsFalse(result.IsValid);
        }

        [Test]
        public void Should_Return_Invalid_Status_When_Contains_Exempt_And_Nonexempt_Directories()
        {
            //arrange
            IList<SvnChangeItemWrapper> changedPaths = new List<SvnChangeItemWrapper>
                                                           {
                                                               new SvnChangeItemWrapper(@"tags/2009.1.0.1/", SvnChangeAction.Modify),
                                                               new SvnChangeItemWrapper(@"trunk/somefile.cs", SvnChangeAction.Modify),

                                                           };            
            ISvnRepository repository = this.CreateRepositoryStub("", "ExemptUser", changedPaths);
            IJiraGateway gateway = this.CreateJiraGatewayStub();
			CommitSentinel sentinel = this.CreateCommitSentinel(repository, gateway);

            //act
            CommitResult result = sentinel.DetermineCommitStatus(TEST_TRANSACTION_ID, TEST_REPOSITORY);

            //assert
            Assert.IsFalse(result.IsValid);
        }

        /// <summary>
        /// exempt directory, but commits fails because of non-exempt user
        /// </summary>
        [Test]
        public void Should_Return_Valid_Status_When_Contains_Exempt_Directory_And_Wildcard_User()
        {
            //arrange
			ISvnRepository repository = this.CreateRepositoryStub("", "joeshmo", new List<SvnChangeItemWrapper> { new SvnChangeItemWrapper(@"/Src/Path/To/Some/Other/Updated/File.cs", SvnChangeAction.Modify) });
            IJiraGateway gateway = this.CreateJiraGatewayStub();
			CommitSentinel sentinel = this.CreateCommitSentinel(repository, gateway);

            //act
            CommitResult result = sentinel.DetermineCommitStatus(TEST_TRANSACTION_ID, TEST_REPOSITORY);

            //assert
            Assert.IsTrue(result.IsValid);
        }


        /// <summary>
        /// Tests that values are retrieved correctly from app.config of test project.
        /// </summary>
        [Test]
        public void Should_Retrieve_Values_From_Custom_Config()
        {
            ExemptItem[] exemptItems =
                CommitPolicyConfigurationSectionHandler.CurrentSettings.CommitExemptionSettings.ExemptItems;

            Assert.AreEqual(3, exemptItems.Length);
			Assert.AreEqual(@"\w*/Src/Path/To/Some/Auto/Updated/File.cs", exemptItems[0].RegularExpressionForPath);
            Assert.AreEqual(1, exemptItems[0].Users.Length);
            Assert.AreEqual("ExemptUser", exemptItems[0].Users[0].Name);
        }

        /// <summary>
        /// issue exists, but closed so prevents commit
        /// </summary>
        [Test]
        public void Should_Return_Invalid_Status_When_Contains_Released_Fixed_Version()
        {
            //arrange
            ISvnRepository repository = this.CreateRepositoryStub("blah PROJ-111 blah");
            IJiraGateway gateway = this.CreateJiraGatewayStub(JiraIssueStatus.Open, true);
			CommitSentinel sentinel = this.CreateCommitSentinel(repository, gateway);

            //act
            CommitResult result = sentinel.DetermineCommitStatus(TEST_TRANSACTION_ID, TEST_REPOSITORY);

            //assert
            Assert.IsFalse(result.IsValid);
        }
        #endregion

        #region Private helpers

        /// <summary>
        /// Creates the commit sentinel.
        /// </summary>
        /// <param name="repository">The repository.</param>
        /// <param name="gateway">The gateway.</param>
        /// <returns></returns>
        private CommitSentinel CreateCommitSentinel(ISvnRepository repository, IJiraGateway gateway)
        {
            ExemptItem exemptItem1 = new ExemptItem
                                         {
                                             Name = "AppVersion",
                                             RegularExpressionForPath =
												 @"\w*/Src/Path/To/Some/Auto/Updated/File.cs",
                                             Users = new[] {new User("ExemptUser")}
                                         };
            ExemptItem exemptItem2 = new ExemptItem
                                         {
                                             Name = "PaymentAuthorizationVersion",
                                             RegularExpressionForPath =
												 @"tags/\w*",
											 Users = new[] { new User("ExemptUser") }
                                         };
            
            CommitExemptionSettings exemptionSettings = new CommitExemptionSettings
                                                            {
                                                                ExemptItems =
                                                                    new[]
                                                                        {
                                                                            exemptItem1, exemptItem2
                                                                        }
                                                            };

            ILog log = MockRepository.GenerateMock<ILog>();
            return CreateCommitSentinel(repository, gateway, log, exemptionSettings);
        }

        /// <summary>
        /// Creates the commit sentinel.
        /// </summary>
        /// <param name="repository">The repository.</param>
        /// <param name="gateway">The gateway.</param>
        /// <param name="log">The log.</param>
        /// <param name="exemptionSettings">The exemption settings.</param>
        /// <returns></returns>
        private CommitSentinel CreateCommitSentinel(ISvnRepository repository, IJiraGateway gateway, ILog log, CommitExemptionSettings exemptionSettings)
        {
            return new CommitSentinel(repository, gateway, log, exemptionSettings);
        }

        /// <summary>
        /// Creates the jira gateway stub.
        /// </summary>
        /// <returns></returns>
        private IJiraGateway CreateJiraGatewayStub()
        {
            return CreateJiraGatewayStub(JiraIssueStatus.Open);
        }

        /// <summary>
        /// Creates the jira gateway stub.
        /// </summary>
        /// <param name="mockedStatus">The mocked status.</param>
        /// <returns></returns>
        private IJiraGateway CreateJiraGatewayStub(JiraIssueStatus mockedStatus)
        {
            return this.CreateJiraGatewayStub(mockedStatus, false);
        }

        /// <summary>
        /// Creates the jira gateway stub.
        /// </summary>
        /// <param name="mockedStatus">The mocked status.</param>
        /// <param name="isFixVersionReleased">if set to <c>true</c> [fix version is released].</param>
        /// <returns></returns>
        private IJiraGateway CreateJiraGatewayStub(JiraIssueStatus mockedStatus, bool isFixVersionReleased)
        {
            IJiraGateway gateway = MockRepository.GenerateStub<IJiraGateway>();
            RemoteVersion fixVersion = new RemoteVersion { name = "2008.4.0.13", released = isFixVersionReleased };
            RemoteIssue issue = new RemoteIssue { status = Convert.ToInt32(mockedStatus).ToString(), fixVersions = new[] { fixVersion } };
            gateway.Stub(x => x.GetJIRAIssue("PROJ-111")).Return(issue);
            return gateway;
        }

        /// <summary>
        /// Creates the repository stub.
        /// </summary>
        /// <param name="logMessage">The log message.</param>
        /// <returns></returns>
        private ISvnRepository CreateRepositoryStub(string logMessage)
        {
            IList<SvnChangeItemWrapper> changedItems = new List<SvnChangeItemWrapper> { new SvnChangeItemWrapper("/some/src/location", SvnChangeAction.Modify)};
            return CreateRepositoryStub(logMessage, "rball", changedItems);
        }

        /// <summary>
        /// Creates the repository stub.
        /// </summary>
        /// <param name="logMessage">The log message.</param>
        /// <param name="author">The author.</param>
        /// <param name="changedItems">The changed items.</param>
        /// <returns></returns>
        private ISvnRepository CreateRepositoryStub(string logMessage, string author, IList<SvnChangeItemWrapper> changedItems)
        {
            ISvnRepository repository = MockRepository.GenerateStub<ISvnRepository>();
            ISvnChangeInfoEventArgsWrapper changeInfo = MockRepository.GenerateStub<ISvnChangeInfoEventArgsWrapper>();

            changeInfo.Stub(x => x.LogMessage).Return(logMessage);
            changeInfo.Stub(x => x.Author).Return(author);
            changeInfo.Stub(x => x.ChangedItems).Return(changedItems);
            //repository.Stub(x => x.GetSvnRevisionInfo(TEST_REPOSITORY, TEST_TRANSACTION_ID)).Return(changeInfo);

            return repository;
        }

        #endregion

    }
}