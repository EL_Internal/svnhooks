using System;
using SVNHooks.Core;

namespace SVNPreCommitHook
{
    /// <summary>
    /// program that is placed in svn hooks directory
    /// </summary>
    class Program
    {
        #region Main Entry Point

        /// <summary>
        /// Mains the specified args.
        /// </summary>
        /// <param name="args">The revision and transaction id's are passed in from SVN</param>
        static void Main(string[] args)
        {
            try
            {
                CommitSentinel sentinel = new CommitSentinel();
                
                CommitResult result = sentinel.DetermineCommitStatus(args[0], args[1], args[2]);
                
                if (result.IsValid)
                    AllowCommit();
                else
                    PreventCommit(result.ErrorMessage);
            }
            catch(System.ServiceModel.EndpointNotFoundException)
            {
                PreventCommit("Unable to connect to Jira.");
            }
            catch (Exception e)
            {
                PreventCommit(e.Message + e.StackTrace);
            }
        }

        #endregion

        #region Private Static

        /// <summary>
        /// The message that is seen in TortoiseSVN when a commit is not allowed
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        private static void PreventCommit(string errorMessage)
        {
            if (String.IsNullOrEmpty(errorMessage) == false)
            {
                Console.Error.WriteLine("****************** COMMIT POLICY VIOLATION *******************");
                Console.Error.WriteLine(errorMessage);
                Console.Error.WriteLine("**************************************************************");
            }

            Environment.Exit(1);
        }

        /// <summary>
        /// Allows the commit by returning expected exit code for success
        /// </summary>
        private static void AllowCommit()
        {
            Environment.Exit(0);
        }

        #endregion
    }
}
